from typing import List


def build_msg(
        label: str,
        count: int,
        plural: str = None,
        head: str = '',
        tail: str = '',
        void: str = 'no',
) -> str:
    """Build a nice count message."""
    if count == 0:
        count_str = void
    else:
        count_str = str(count)
    label_str = build_plural(label=label, count=count, plural=plural)
    msg_items: List[str] = [head, count_str, label_str, tail]
    return ' '.join(item for item in msg_items if item)


def build_plural(label: str, count: int, plural: str = None) -> str:
    """Build a plural form of *label*."""
    if count > 1:
        if plural is not None:
            return plural
        else:
            return label + 's'
    else:
        return label
