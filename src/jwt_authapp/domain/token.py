import datetime
from typing import Dict

import jwt

FAKE_SECRET_KEY: str = 'some key'
VALIDITY_DURATION: float = 5.0  #: seconds of token validity

TokenType = str


class TokenError(Exception):
    """Exception to signal token encoding or decoding error."""


class ExpiredToken(TokenError):
    """Exception to signal an expired token."""


class InvalidToken(TokenError):
    """Exception to signal invalid token."""


class TokenManager:
    """JWT Token manager.

    An instance of this class will be able to encode tokens, and to decode
    tokens it encoded.

    The validity duration is tunable.

    """

    def __init__(
            self,
            secret_key: str = FAKE_SECRET_KEY,
            validity_duration: float = VALIDITY_DURATION,
    ):
        """Initialize the token manager.

        Args:
            secret_key: key that will be used to encode the tokens. Keep
                this absolutely secret!
            validity_duration: time in seconds during which a token will be
                valid. Can be set to a negative value for testing purpose.

        """
        self.secret_key = secret_key
        self._validity_duration = validity_duration

    @property
    def validity_duration(self) -> datetime.timedelta:
        return datetime.timedelta(days=0, seconds=self._validity_duration)

    @validity_duration.setter
    def validity_duration(self, validity_duration: float):
        self._validity_duration = validity_duration

    def encode(self, user_id: int) -> str:
        """Encode *user_id* in a JWT token.

        Returns:
            Token, as a unicode string (not as bytes).

        This token can then be used as a proof that the user was correctly
        authenticated in the past.

        """
        now = datetime.datetime.utcnow()
        payload = dict(sub=user_id, iat=now, exp=now + self.validity_duration)
        return jwt.encode(payload, self.secret_key, algorithm='HS256').decode()

    def decode(self, token: str) -> Dict:
        """Decode *token*, using the initial secret key.

        Args:
            token: JSON Web Token, as a unicode string (not as bytes).

        Returns:
            Decoded token content.

        Raises:
            TokenSignatureError: if the token is either expired or invalid.

        """
        try:
            return jwt.decode(token, self.secret_key)
        except jwt.InvalidSignatureError:
            raise InvalidToken("Token is invalid and cannot be used")
        except jwt.ExpiredSignatureError:
            raise ExpiredToken("Token is expired and cannot be used anymore")
