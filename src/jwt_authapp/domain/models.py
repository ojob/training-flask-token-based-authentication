from __future__ import annotations

from dataclasses import dataclass, field
from datetime import datetime
from hashlib import sha256
from typing import Dict, Set

from .crypto import PasswordManager, FakePasswordManager


_id_gen = (n for n in range(10**10))


@dataclass
class User:
    email: str
    password: str
    id: int = field(default_factory=lambda: next(_id_gen))
    registered_on: datetime = field(default_factory=datetime.now)
    is_admin: bool = False
    groups: Set[Group] = field(default_factory=set)

    def as_dict(self) -> Dict:
        return dict(
            id=self.id, email=self.email, is_admin=self.is_admin,
            registered_on=self.registered_on,
            groups=[group.as_dict() for group in self.groups])

    def public_details(self) -> Dict:
        return dict(id=self.id, email=self.email)

    def __eq__(self, other) -> bool:
        if other.__class__ is self.__class__:
            return self.id == other.id \
                and self.registered_on == other.registered_on
        return NotImplemented

    def __gt__(self, other) -> bool:
        if other.__class__ is self.__class__:
            return self.id > other.id
        return NotImplemented

    def __hash__(self) -> int:
        return hash(f'User{self.id},created{self.registered_on}')


class UserFactory:
    """Class for handling User instances.

    In particular, this class handles the cryptographic methods, as the
    User instances only store the hashed values but are not in charge of
    handling the methods for cyphering and comparison to plain text
    candidates.

    """
    def __init__(self, pw_manager: PasswordManager = None):
        if pw_manager is None:
            pw_manager = FakePasswordManager()
        self.pw_manager = pw_manager

    def create(
            self,
            email: str,
            password: str,
            is_admin: bool = False,
    ) -> User:
        return User(
            email=email,
            password=self.pw_manager.hash(password),
            is_admin=is_admin,
        )

    def check_user_password(
            self,
            user: User,
            candidate_password: str,
    ) -> bool:
        return self.pw_manager.check(candidate_password, user.password)


@dataclass
class BlacklistedToken:
    token_hash: str
    blacklisted_on: datetime = field(default_factory=datetime.now)

    @classmethod
    def from_token(cls, token: str) -> BlacklistedToken:
        return cls(
            token_hash=sha256(token.encode()).hexdigest(),
        )


@dataclass
class Group:
    name: str
    created_by: User
    description: str
    id: int = field(default_factory=lambda: next(_id_gen))
    is_private: bool = False
    created_on: datetime = field(default_factory=datetime.now)
    users: Set[User] = field(default_factory=set)

    def as_dict(self) -> Dict:
        return dict(id=self.id, name=self.name, description=self.description)

    def is_visible(self, user: User = None) -> bool:
        if self.is_private:
            return user in self.users
        return True

    def __eq__(self, other) -> bool:
        if self.__class__ is other.__class__:
            return self.id == other.id and self.created_on == other.created_on
        return NotImplemented

    def __hash__(self) -> int:
        return hash(f'Group{self.id},created{self.created_on}')


class GroupFactory:
    """Class for handling Group instances.

    This class exists primarily for symmetry with :py:class:`UserFactory`,
    but does not do much.

    """

    @staticmethod
    def create(
            name: str,
            created_by: User,
            id: int = None,
            description: str = None,
            created_on: datetime = None,
    ) -> Group:
        group_data = dict(name=name, created_by=created_by)
        group_data['users'] = {created_by}
        group_data['description'] = description or ''
        if id is not None:
            group_data['id'] = id
        if created_on is not None:
            group_data['created_on'] = created_on
        return Group(**group_data)   # type: ignore


class MembershipError(Exception):
    """Class to handle user memberships to groups errors."""
