import abc

import bcrypt


class PasswordManager(abc.ABC):  # pragma: no cover
    @abc.abstractmethod
    def hash(self, clear_password: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def check(self, candidate_password: str, password_hash: str) -> bool:
        raise NotImplementedError()


class FakePasswordManager(PasswordManager):
    def hash(self, clear_password):
        return self.transform(clear_password)

    def check(self, candidate_password, password_hash):
        return self.transform(candidate_password) == password_hash

    @staticmethod
    def transform(clear_password: str) -> str:
        return f'==hashed=={clear_password}=='


class BcryptPasswordManager(PasswordManager):
    def __init__(self, bcrypt_rounds: int = 14):
        self.rounds = bcrypt_rounds

    def hash(self, clear_password):
        return bcrypt.hashpw(
            clear_password.encode('utf-8'),
            bcrypt.gensalt(self.rounds)
        ).decode('utf-8')

    def check(self, candidate_password, password_hash):
        return bcrypt.checkpw(
            candidate_password.encode('utf-8'),
            password_hash.encode('utf-8'),
        )
