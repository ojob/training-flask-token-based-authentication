from typing import List

from ..domain.models import GroupFactory, MembershipError, User, Group
from ..repository import Repository
from . import Service


class NoGroupFound(Exception):
    """When no group is found, but one (or more) is expected."""


class GroupService(Service):

    def __init__(
            self,
            repo: Repository,
    ):
        """Initialize."""
        self.repo = repo
        self.group_factory = GroupFactory()

    def create_group(
            self,
            name: str,
            created_by: User,
            description: str = None,
    ) -> Group:
        """Create a new group from its characteristics, and persist it."""
        group = self.group_factory.create(
            name=name, description=description, created_by=created_by)
        self.repo.add_group(group)
        return group

    def _add_group(self, group: Group) -> None:
        """Add an existing *group* to the repo.

        This is a method to be used for test purpose only, as it bypasses
        checks at group creation.

        """
        self.repo.add_group(group=group)

    def add_user_to_group(self, user: User, guest: User, group: Group) -> None:
        """Add *user* to the users of *group*, and persist this."""
        repo_group: Group = self.repo.get_group(group_id=group.id)
        if user not in repo_group.users:
            raise MembershipError(
                f"user {user.id} is not a member of group {group.id}")
        repo_guest: User = self.repo.get_user(user_id=guest.id)
        if repo_guest not in repo_group.users:
            repo_group.users.add(repo_guest)

    def get_groups(
            self, *,  # accepting only keyword parameters
            user: User = None,
    ) -> List[Group]:
        return [group for group in self.repo.get_groups(user=user)
                if group.is_visible(user=user)]

    def get_group(self, group_id: int) -> Group:
        """Return group """
        return self.repo.get_group(group_id=group_id)

    def commit(self):
        self.repo.commit()

    def rollback(self):
        self.repo.rollback()

    def close(self):
        self.repo.close()
