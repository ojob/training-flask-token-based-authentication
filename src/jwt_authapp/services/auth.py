from __future__ import annotations

from typing import Dict, List, Optional

from ..domain.crypto import PasswordManager, BcryptPasswordManager
from ..domain.models import User, UserFactory
from ..domain.token import TokenManager, TokenError, TokenType
from ..repository import NoResultFound, Repository
from . import Service


class UserLoginFailure(Exception):
    """Exception for failure to authenticate user."""


class UserRegistrationDuplicate(Exception):
    """Exception for User registration duplicate."""


class BlacklistedToken(TokenError):
    """Additional exception to signal a blacklisted token."""


class AuthService(Service):
    """Class to manage authentication.

    This gathers several functionalities by composition, and should be the
    sole entry point from peripheral concerns, like web or programmatic usage.

    """

    def __init__(
            self,
            repo: Repository,
            user_factory: UserFactory = None,
            pw_manager: PasswordManager = None,
            token_manager: TokenManager = None,
            token_validity: float = None,
    ):
        """Initialize the authentication service.

        This is your chance to configure the internals!

        """
        self.repo = repo
        self.pw_manager = pw_manager or BcryptPasswordManager()
        self.user_factory = user_factory or UserFactory(self.pw_manager)
        self.token_manager = token_manager or TokenManager()
        if token_validity is not None:
            self.token_manager.validity_duration = token_validity

    def create_user(self, email, password) -> User:
        """Create a new group from its characteristics, and persist it."""
        if self.find_user(email=email) is not None:
            raise UserRegistrationDuplicate(
                f"user with email={email} already registered")
        user = self.user_factory.create(email=email, password=password)
        self._add_user(user)
        return user

    def _add_user(self, user: User):
        """Add an existing *user* to the repo.

        This is a method to be used for test purpose only, as it bypasses
        checks at user creation.

        """
        self.repo.add_user(user)

    def log_user(self, email: str, password: str) -> TokenType:
        """Log the user based on provided email and password.

        Args:
            email: user email, used as authentication key.
            password: plain password value as entered by the
                system trying to authenticate.

        Returns:
            token authenticating the user, if credentials are correct.

        Raises:
            UserRegistrationFailure: if email is not known, or password
                does not match.

        """
        try:
            user = self.repo.get_user(email=email)
        except NoResultFound:
            raise UserLoginFailure("unknown user or incorrect password")
        if user is None or not self.user_factory.check_user_password(
                user=user,
                candidate_password=password):
            raise UserLoginFailure("unknown user or incorrect password")
        return self.token_manager.encode(user.id)

    def get_users(self) -> List[User]:
        return self.repo.get_users()

    def get_user(
            self, *,  # accepting keyword-calls only
            user_id: int = None,
            token: TokenType = None,
            email: str = None,
    ) -> User:
        """Get the user corresponding to the criterion.

        Raises:
            NoResultFound: if no user matches the criterion.
            TypeError: if no criterion is provided.

        """
        if user_id is not None or email is not None:
            user: User = self.repo.get_user(user_id=user_id, email=email)
        elif token is not None:
            user = self._get_user_by_token(token=token)
        else:
            raise TypeError("one of user_id, email or token shall be provided")
        return user

    def find_user(
            self, *,  # accepting keyword-calls only
            user_id: int = None,
            email: str = None,
    ) -> Optional[User]:
        return self.repo.find_user(user_id=user_id, email=email)

    def _get_user_by_token(self, token: TokenType) -> User:
        self.validate(token)
        user_id: int = self.token_manager.decode(token)['sub']
        return self.repo.get_user(user_id=user_id)

    def get_token(self, user: User) -> TokenType:
        return self.token_manager.encode(user.id)

    def decode(self, token: TokenType) -> Dict:
        return self.token_manager.decode(token)

    def validate(self, token: TokenType) -> None:
        """Validate the token, for validity and activity.

        Returns:
            Nothing

        Raises:
            ExpiredToken:
            InvalidToken:
            BlacklistedToken:

        """
        if self.is_blacklisted(token):  # quickest check first
            raise BlacklistedToken(
                "Token is blacklisted and cannot be used anymore")
        self.token_manager.decode(token)  # raises TokenError

    def is_blacklisted(self, token: TokenType) -> bool:
        return self.repo.is_blacklisted(token)

    def blacklist(self, token: TokenType):
        try:
            self.validate(token)
        except TokenError:
            pass
        else:
            # blacklist only if the token is valid
            self.repo.blacklist(token)

    def commit(self):
        self.repo.commit()

    def rollback(self):
        self.repo.rollback()
