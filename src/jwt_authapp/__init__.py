from jwt_authapp.web_api.app import create_app


def run(port: int = 5004):
    app = create_app()
    app.run(port=port)
