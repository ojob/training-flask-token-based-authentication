import os
from typing import Tuple

FlaskAnswer = Tuple[str, int]

DEFAULT_CONFIG = 'jwt_authapp.config.DevelopmentConfig'

app_settings = os.getenv('APP_SETTINGS', DEFAULT_CONFIG)
