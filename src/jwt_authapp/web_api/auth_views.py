import functools
from typing import Callable, Dict

from flask import Blueprint, jsonify, request

from ..domain.token import TokenError, TokenType
from ..domain.models import User
from ..services.auth import AuthService, UserLoginFailure, \
    UserRegistrationDuplicate
from . import FlaskAnswer


class AuthenticationError(Exception):
    """Exception to signal an error with Authentication in views."""


class AuthorizationFormatError(AuthenticationError):
    """Exception to signal a format error with Authorization."""


class AuthorizationHeaderMissing(AuthenticationError):
    """Exception to signal absence of Authorization (sic) header."""


def get_token_from_header(headers: Dict) -> TokenType:
    """Extract the JSON Web Token from the request.

    This function makes no validation: it just parses the request header.

    Raises:
        AuthenticationFormatError: if the token cannot be extracted.

    """
    try:
        auth_header: str = headers['Authorization']
    except KeyError:
        raise AuthorizationHeaderMissing(
            "Authorization header required for this resource but not present; "
            "please add it with the token, like 'Bearer <token>'")

    if not auth_header.startswith('Bearer '):
        raise AuthorizationFormatError(
            "Authorization header format error; "
            "shall be like 'Bearer <token>'")

    token: TokenType = auth_header[len('Bearer '):]
    if not token:
        raise AuthorizationFormatError(
            "Authorization header contains no token; "
            "shall be like 'Bearer <token>'")
    return token


class AuthAPI:
    """Provides entry points to handle users and related tokens.

    Usage:

    - instantiate this class with an :py:class:`AuthService` instance
    -
    - decorate your views with either:

        - :py:meth:`inject_token`
        - :py:meth:`require_valid_token`
        - :py:meth:`inject_user`

    """

    def __init__(self, auth_service: AuthService):
        self.auth_service = auth_service
        self.status = self.inject_token(self._status)
        self.logout = self.inject_token(self._logout)

    # --------------------------------------------------------- Flask views
    def register(self) -> FlaskAnswer:
        user_data = request.get_json()
        try:
            user: User = self.auth_service.create_user(**user_data)
            self.auth_service.commit()
        except UserRegistrationDuplicate:
            payload = dict(
                message="user already registered with this email",
            )
            status = 403
        else:
            token: str = self.auth_service.get_token(user)
            payload = dict(
                message="user created",
                data=dict(
                    id=user.id,
                    token=token,
                ),
            )
            status = 201
        return jsonify(payload), status

    def login(self) -> FlaskAnswer:
        user_data = request.get_json()
        try:
            token: str = self.auth_service.log_user(**user_data)
        except UserLoginFailure:
            payload = dict(
                message="unknown email or wrong password",
            )
            status = 401
        else:
            payload = dict(
                message="successfully logged in",
                data=dict(token=token),
            )
            status = 200
        return jsonify(payload), status

    def _status(self, token: TokenType) -> FlaskAnswer:
        payload = dict(
            data=self.auth_service.decode(token)
        )
        return jsonify(payload), 200

    def _logout(self, token: TokenType) -> FlaskAnswer:
        self.auth_service.blacklist(token)
        self.auth_service.commit()
        payload = dict(
            message='token blacklisted',
        )
        status = 200
        return jsonify(payload), status

    # ---------------------------------------------------------- decorators
    def inject_token(self, func) -> Callable:
        """Decorate *func* to auto-magically get the request token.

        Use it as a decorator over a view than needs a valid token as input.

        If the token is not valid:

        - if the token cannot be extracted, then 400 Bad Request is returned
        - if the token is expired, then 401 Unauthorized is returned
        - if the token is blacklist, then 410 Gone is returned

        """

        def wrapped_func(*args, **kwargs) -> FlaskAnswer:
            try:
                token: TokenType = get_token_from_header(request.headers)
                self.auth_service.validate(token)
            except AuthorizationHeaderMissing as exc:
                payload = dict(message=f'{exc}')
                status = 401  # Unauthorized
            except AuthenticationError as exc:
                payload = dict(message=f'{exc}')
                status = 400  # Bad Request
            except TokenError as exc:
                payload = dict(message=f'{exc}; please log in')
                status = 401  # Unauthorized
            else:
                return func(*args, token=token, **kwargs)
            return jsonify(payload), status

        functools.update_wrapper(wrapped_func, func)
        return wrapped_func

    def inject_user(self, func) -> Callable:
        """Decorate *func* to auto-magically get the user from request token.

        Use it as a decorator over a view than needs the user as input.

        The token is guaranteed to be valid by the usage of
        :py:meth:`inject_token`; this decorator also handles the failure
        response.

        """
        @self.inject_token
        def wrapped_func(token: TokenType, *args, **kwargs) -> FlaskAnswer:
            # Retrieve the user from the token. If this fails, then # *func*
            # is not called.
            user = self.auth_service.get_user(token=token)
            return func(*args, user=user, **kwargs)

        functools.update_wrapper(wrapped_func, func)
        return wrapped_func

    def auth_required(self, func) -> Callable:
        """Decorate *func* to check that user is correctly authenticated.

        This is the same code as :py:meth:`inject_user`, but the user is not
        injected in *func*. This enables to decorate a function just to check
        that someone is authenticated, with *func* not doing anything with
        the current user object instance.

        """
        @self.inject_token
        def wrapped_func(token: TokenType, *args, **kwargs) -> FlaskAnswer:
            # just try to call the user from the token. If this fails, then
            # *func* is not called.
            _ = self.auth_service.get_user(token=token)
            return func(*args, **kwargs)

        functools.update_wrapper(wrapped_func, func)
        return wrapped_func


def create_auth_blp(auth_handler: AuthAPI) -> Blueprint:
    auth_blueprint = Blueprint('auth', __name__)

    auth_blueprint.add_url_rule(
        '/register',
        view_func=auth_handler.register,
        methods=['POST'],
    )
    auth_blueprint.add_url_rule(
        '/login',
        view_func=auth_handler.login,
        methods=['POST'],
    )
    auth_blueprint.add_url_rule(
        '/status',
        view_func=auth_handler.status,
        methods=['GET'],
    )
    auth_blueprint.add_url_rule(
        '/logout',
        view_func=auth_handler.logout,
        methods=['POST'],
    )

    return auth_blueprint
