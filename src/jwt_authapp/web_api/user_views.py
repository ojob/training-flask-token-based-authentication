from typing import List

from flask import Blueprint, jsonify

from ..domain.models import User
from ..services.auth import AuthService
from ..utils import build_msg
from . import FlaskAnswer, auth_views


class UsersAPI:

    def __init__(self, auth_service: AuthService):
        self.auth_service = auth_service

    def get_users(self) -> FlaskAnswer:
        users: List[User] = self.auth_service.get_users()
        payload = dict(
            message=build_msg(head="found", count=1, label="user"),
            data=[user.as_dict() for user in users],
        )
        return jsonify(payload), 200

    @staticmethod
    def get_current_user_details(user: User) -> FlaskAnswer:
        payload = dict(
            data=user.as_dict(),
        )
        return jsonify(payload), 200


def create_users_blp(
        auth_handler: auth_views.AuthAPI,
) -> Blueprint:
    users_blueprint = Blueprint('users', __name__)

    users_api = UsersAPI(auth_service=auth_handler.auth_service)

    users_blueprint.add_url_rule(
        '/',
        view_func=auth_handler.auth_required(users_api.get_users),
        methods=['GET'],
    )
    users_blueprint.add_url_rule(
        '/me',
        view_func=auth_handler.inject_user(users_api.get_current_user_details),
        methods=['GET'],
    )

    return users_blueprint
