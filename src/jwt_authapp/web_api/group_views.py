from typing import Dict, List

from flask import Blueprint, jsonify, request

from ..domain.models import Group, User
from ..repository import NoResultFound
from ..services.auth import AuthService
from ..services.group import GroupService
from ..utils import build_msg
from . import FlaskAnswer, auth_views


class GroupsAPI:

    def __init__(
            self,
            auth_service: AuthService,
            group_service: GroupService,
    ):
        self.auth_service = auth_service
        self.group_service = group_service

    def get_group(self, group_id: int) -> FlaskAnswer:
        try:
            group: Group = self.group_service.get_group(group_id=group_id)
        except NoResultFound as exc:
            payload = dict(
                message=str(exc),
            )
            status = 404
        else:
            payload = dict(
                data=dict(
                    created_by=group.created_by.public_details(),
                    created_on=group.created_on,
                ),
            )
            status = 200
        return jsonify(payload), status

    def get_groups(self) -> FlaskAnswer:
        """Show all groups"""
        groups: List[Group] = self.group_service.get_groups()
        payload = dict(
            message=build_msg(count=len(groups), label='group', tail='found'),
            data=[group.as_dict() for group in groups],
        )
        self.group_service.close()
        return jsonify(payload), 200

    def get_my_groups(self, user: User) -> FlaskAnswer:
        groups: List[Group] = self.group_service.get_groups(user=user)
        payload = dict(
            message=build_msg(
                head='user is part of', count=len(groups), label='group'),
            data=[group.as_dict() for group in groups],
        )
        self.group_service.close()
        return jsonify(payload), 200

    def create_group(self, user: User) -> FlaskAnswer:
        """Create and store a new group."""
        # gather group info from the request
        group_data = dict(
            name=request.json['name'],
            description=request.json['description'],
        )

        # create and store the group
        group: Group = self.group_service.create_group(
            **group_data, created_by=user)
        self.group_service.commit()

        # build response to show correct group creation
        payload: Dict = dict(
            message="group created",
            data=group.as_dict(),
        )
        self.group_service.close()
        return jsonify(payload), 201

    def get_group_users(
            self,
            group_id: int,
    ) -> FlaskAnswer:
        try:
            group: Group = self.group_service.get_group(group_id=group_id)
        except NoResultFound as exc:
            payload = dict(
                message=str(exc),
            )
            status = 404
        else:
            payload = dict(
                message=build_msg(
                    head="found", count=len(group.users), label="user"),
                data=[user.public_details() for user in group.users],
            )
            status = 200
        return jsonify(payload), status

    def add_user_to_group(
            self,
            user: User,
            group_id: int,
    ) -> FlaskAnswer:
        """Add user corresponding to *user_id* as member of *group_id*.

        *user* shall be injected by Authentication, i.e. this function acts
        as an invitation.

        """
        user_id: int = request.json.get('user_id')
        try:
            group: Group = self.group_service.get_group(group_id=group_id)
            guest: User = self.auth_service.get_user(user_id=user_id)
        except NoResultFound as exc:
            payload = dict(
                message=str(exc),
            )
            status = 404
        else:
            self.group_service.add_user_to_group(
                user=user, guest=guest, group=group)
            self.group_service.commit()
            payload = dict(
                message=f'user {guest.email} associated to group {group.name}',
            )
            status = 200
        return jsonify(payload), status


def create_groups_blp(
        group_service: GroupService,
        auth_handler: auth_views.AuthAPI,
) -> Blueprint:
    groups_blueprint = Blueprint('groups', __name__)

    groups_api = GroupsAPI(
        auth_service=auth_handler.auth_service,
        group_service=group_service,
    )

    groups_blueprint.add_url_rule(
        '/',
        view_func=groups_api.get_groups,
        methods=['GET'],
    )
    groups_blueprint.add_url_rule(
        '/',
        view_func=auth_handler.inject_user(groups_api.create_group),
        methods=['POST'],
    )
    groups_blueprint.add_url_rule(
        '/<int:group_id>',
        view_func=groups_api.get_group,
        methods=['GET'],
    )
    groups_blueprint.add_url_rule(
        '/<int:group_id>/users',
        view_func=auth_handler.auth_required(groups_api.get_group_users),
        methods=['GET'],
    )
    groups_blueprint.add_url_rule(
        '/<int:group_id>/users',
        view_func=auth_handler.inject_user(groups_api.add_user_to_group),
        methods=['POST'],
    )
    groups_blueprint.add_url_rule(
        '/mine',
        view_func=auth_handler.inject_user(groups_api.get_my_groups),
        methods=['GET'],
    )

    return groups_blueprint
