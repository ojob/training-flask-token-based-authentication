from flask import Flask, jsonify


from ..repository import Repository
from ..repository.pupy import DictRepository
from ..services.auth import AuthService
from ..services.group import GroupService
from . import FlaskAnswer, app_settings
from .auth_views import AuthAPI, create_auth_blp
from .group_views import create_groups_blp
from .user_views import create_users_blp


def create_app(
        repo: Repository = None,
        token_validity: float = None,
) -> Flask:
    """Create the application Flask instance.

    This function attaches the blueprints created around.

    """
    app = Flask(__name__)
    app.config.from_object(app_settings)

    app.repo = repo or DictRepository()
    app.repo.init()
    app.auth_service = AuthService(
        repo=app.repo,
        token_validity=token_validity,
    )
    auth_handler = AuthAPI(app.auth_service)
    group_service = GroupService(
        repo=app.repo,
    )

    app.register_blueprint(
        create_auth_blp(auth_handler=auth_handler),
        url_prefix='/auth',
    )
    app.register_blueprint(
        create_users_blp(auth_handler=auth_handler),
        url_prefix='/users',
    )
    app.register_blueprint(
        create_groups_blp(
            group_service=group_service,
            auth_handler=auth_handler,
        ),
        url_prefix='/groups'
    )

    @app.route('/healthy')
    def healthy() -> FlaskAnswer:
        return jsonify(dict(message='up and running!')), 200

    return app
