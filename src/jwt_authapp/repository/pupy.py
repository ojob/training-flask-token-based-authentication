from typing import Any, Dict, List, Tuple

from ..domain.models import BlacklistedToken, Group, User
from ..domain.token import TokenType
from . import NoResultFound, Repository


class DictRepository(Repository):

    def __init__(self):
        super().__init__()
        self._users_by_id: Dict[int, User] = {}
        self._users_by_email: Dict[str, User] = {}
        self._groups_by_id: Dict[int, Group] = {}
        self._id_gen = (n for n in range(10**10))
        self._tokens_blacklist: Dict[str, BlacklistedToken] = {}

        self._added_items: List[Tuple[Any, dict]] = []

    def init(self) -> None:
        pass  # no initialization with side-effect

    def commit(self) -> None:
        self._added_items = []

    def rollback(self) -> None:
        for item_id, item_store in self._added_items:
            del item_store[item_id]

    def close(self) -> None:
        pass  # nothing to close when ending a session

    def add_user(self, user: User) -> int:
        self.logger.warning("nb of users stored: %d" % len(self._users_by_id))
        user.id = next(self._id_gen)
        self._users_by_id[user.id] = user
        self._users_by_email[user.email] = user
        self._added_items.append((user.id, self._users_by_id))
        self._added_items.append((user.email, self._users_by_email))
        return user.id

    def get_users(self) -> List[User]:
        return list(self._users_by_id.values())

    def _get_user_by_id(self, user_id: int) -> User:
        try:
            return self._users_by_id[user_id]
        except KeyError:
            raise NoResultFound(f"no user found with id={user_id}")

    def _get_user_by_email(self, email: str) -> User:
        try:
            return self._users_by_email[email]
        except KeyError:
            raise NoResultFound(f"no user found with email={email}")

    def blacklist(self, token: TokenType) -> None:
        blacklist_entry = BlacklistedToken.from_token(token)
        self._tokens_blacklist[blacklist_entry.token_hash] = blacklist_entry

    def is_blacklisted(self, token: TokenType) -> bool:
        blacklist_entry = BlacklistedToken.from_token(token)
        return blacklist_entry.token_hash in self._tokens_blacklist

    def add_group(self, group: Group) -> None:
        group.id = next(self._id_gen)
        self._groups_by_id[group.id] = group
        self._added_items.append((group.id, self._groups_by_id))

    def get_groups(self, user: User = None) -> List[Group]:
        all_groups = self._groups_by_id.values()
        if user is not None:
            res = [group for group in all_groups if user in group.users]
        else:
            res = list(all_groups)
        return res

    def _get_group_by_id(self, group_id: int) -> Group:
        try:
            return self._groups_by_id[group_id]
        except KeyError:
            raise NoResultFound(f"no group found with id={group_id}")
