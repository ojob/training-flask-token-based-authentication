from typing import List

from sqlalchemy.orm import Session
from sqlalchemy.orm import exc

from ..domain.models import BlacklistedToken, Group, User
from ..domain.token import TokenType
from ..repository import NoResultFound, Repository


class SQLRepository(Repository):

    def __init__(self, session: Session):
        super().__init__()
        self.session: Session = session

    def init(self) -> None:
        pass

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()

    def close(self) -> None:
        self.session.close()

    def add_user(self, user) -> None:
        self.session.add(user)

    def get_users(self) -> List[User]:
        return self.session.query(User).all()

    def _get_user_by_id(self, user_id: int) -> User:
        try:
            return self.session.query(User).filter_by(id=user_id).one()
        except exc.NoResultFound:
            raise NoResultFound(f"no user found with id={user_id}")

    def _get_user_by_email(self, email: str) -> User:
        try:
            return self.session.query(User).filter_by(email=email).one()
        except exc.NoResultFound:
            raise NoResultFound(f"no user found with email={email}")

    def blacklist(self, token: TokenType) -> None:
        token_blacklist_entry = BlacklistedToken.from_token(token)
        self.session.add(token_blacklist_entry)

    def is_blacklisted(self, token: TokenType) -> bool:
        blacklist_entry = BlacklistedToken.from_token(token)
        item = self.session.query(BlacklistedToken)\
            .filter_by(token_hash=blacklist_entry.token_hash)\
            .first()
        return item is not None

    def add_group(self, group: Group) -> None:
        self.session.add(group)

    def get_groups(self, user: User = None) -> List[Group]:
        query = self.session.query(Group)
        if user is not None:  # filtering by *user* is requested
            query = query.join(Group.users).filter(User.id == user.id)
        return query.all()

    def _get_group_by_id(self, group_id: int) -> Group:
        try:
            return self.session.query(Group).filter_by(id=group_id).one()
        except exc.NoResultFound:
            raise NoResultFound(f"no group found with id={group_id}")
