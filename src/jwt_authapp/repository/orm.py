import sqlalchemy as sa
from sqlalchemy.orm import mapper, relationship

from jwt_authapp.domain.models import Group, User, BlacklistedToken


metadata = sa.MetaData()
users = sa.Table(
    'users', metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('email', sa.String(255), unique=True, nullable=False),
    sa.Column('password', sa.String(255), nullable=False),
    sa.Column('registered_on', sa.DateTime, nullable=False),
    sa.Column('is_admin', sa.Boolean, nullable=False, default=False),

)
tokens_blacklist = sa.Table(
    'tokens_blacklist', metadata,
    sa.Column('token_hash', sa.String(56), primary_key=True),
    sa.Column('blacklisted_on', sa.DateTime, nullable=False),
)
groups = sa.Table(
    'groups', metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.String(60), unique=True, nullable=False),
    sa.Column('description', sa.String(500), nullable=True),
    sa.Column('is_private', sa.Boolean, nullable=False, default=False),
    sa.Column(
        'created_by_id', sa.Integer, sa.ForeignKey('users.id'), nullable=False),
    sa.Column('created_on', sa.DateTime, nullable=False),
)
users_groups_association = sa.Table(
    # membership
    'users_groups_association', metadata,
    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('user_id', sa.ForeignKey('users.id')),
    sa.Column('group_id', sa.ForeignKey('groups.id')),
)


def start_mappers():
    """Add SQLAlchemy stuff inside the model classes."""
    mapper(User, users, properties=dict(
        groups=relationship(
            Group, secondary=users_groups_association, back_populates='users',
            collection_class=set,
        ),
        created_groups=relationship(
            Group, backref='created_by', collection_class=set,
        )
    ))
    mapper(Group, groups, properties=dict(
        users=relationship(
            User, secondary=users_groups_association, back_populates='groups',
            collection_class=set,
        ),
    ))

    mapper(BlacklistedToken, tokens_blacklist)
