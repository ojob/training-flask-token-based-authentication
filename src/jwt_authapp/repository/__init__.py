import abc
import logging
from typing import List, Optional

from jwt_authapp.domain.models import Group, User


class NoResultFound(Exception):
    """When one or more results are expected, but non is found."""


class Repository(abc.ABC):  # pragma: no cover
    """Base class for repository.

    :py:meth:`init` is called by the services, to initialize the repository.

    After a request to the repo, :py:meth:`close` is called at the end of
    the request, after either :py:meth:`commit` or :py:meth:`rollback`.

    """

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    @abc.abstractmethod
    def init(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def commit(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def rollback(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def close(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def add_user(self, user: User) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    def get_users(self) -> List[User]:
        raise NotImplementedError()

    def get_user(self, *, user_id: int = None, email: str = None) -> User:
        if user_id is not None:
            return self._get_user_by_id(user_id=user_id)
        elif email is not None:
            return self._get_user_by_email(email=email)
        else:
            raise TypeError("one of user_id, email shall be provided")

    @abc.abstractmethod
    def _get_user_by_id(self, user_id: int) -> User:
        raise NotImplementedError()

    @abc.abstractmethod
    def _get_user_by_email(self, email: str) -> User:
        raise NotImplementedError()

    def find_user(
            self, *,
            user_id: int = None,
            email: str = None,
    ) -> Optional[User]:
        try:
            return self.get_user(user_id=user_id, email=email)
        except NoResultFound:
            return None

    @abc.abstractmethod
    def blacklist(self, token: str) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    def is_blacklisted(self, token: str) -> bool:
        raise NotImplementedError()

    @abc.abstractmethod
    def add_group(self, group: Group) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    def get_groups(self, user: User = None) -> List[Group]:
        raise NotImplementedError()

    def get_group(self, *, group_id: int) -> Group:
        return self._get_group_by_id(group_id=group_id)

    @abc.abstractmethod
    def _get_group_by_id(self, *, group_id: int) -> Group:
        raise NotImplementedError()
