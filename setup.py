from setuptools import setup, find_packages

setup(
    name="jwt-authapp",
    version="0.0",

    description="A back-end to demonstrate handling of JSON web tokens",

    packages=find_packages('src'),
    package_dir={'': 'src'},

    install_requires=[
        'bcrypt',
        'flask',
        'pyjwt',
        'sqlalchemy',
    ],
)
