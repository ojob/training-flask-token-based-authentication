from typing import List, Tuple

import pytest

from jwt_authapp.domain.models import Group, MembershipError, User
from jwt_authapp.repository import Repository
from jwt_authapp.services.auth import AuthService
from jwt_authapp.services.group import GroupService


@pytest.fixture
def group_service(repo: Repository) -> GroupService:
    return GroupService(repo=repo)


@pytest.fixture
def auth_and_group_service(
        repo: Repository,
) -> Tuple[AuthService, GroupService]:
    return AuthService(repo=repo), GroupService(repo=repo)


def test_empty_group_service(group_service):
    # given a new group service

    # when groups are requested
    groups: List[Group] = group_service.get_groups()

    # then no group is returned
    assert isinstance(groups, list)
    assert len(groups) == 0


def test_group_creation_commit_and_persistence(auth_and_group_service):
    auth_service, group_service = auth_and_group_service
    # given some group data and a user
    group_data = dict(name='some group', description='description')
    user_data = dict(email='lea@some.place', password='123')

    # when group creation is requested
    user = auth_service.create_user(**user_data)
    group_service.create_group(**group_data, created_by=user)
    group_service.commit()

    # then request for groups returns the created group
    groups: List[Group] = group_service.get_groups()
    assert len(groups) == 1
    assert groups[0].name == group_data['name']
    assert isinstance(groups[0].users, set)
    user0 = next(user for user in groups[0].users)
    assert user0.id == user.id
    assert user0.email == user.email
    assert user0.email == user.email


def test_group_creation_rollback_and_no_persistence(group_service):
    # given some group data and a user
    group_data = dict(name='some group', description='description')
    user = User(email='lea@some.place', password='123')

    # when group creation is requested but rollback is requested
    group_service.create_group(**group_data, created_by=user)
    group_service.rollback()

    # then request for groups returns no group
    groups: List[Group] = group_service.get_groups()
    assert len(groups) == 0


def test_group_user_addition_and_request(auth_and_group_service):
    auth_service, group_service = auth_and_group_service
    # given some groups and users
    users: List[User] = \
        [User(email=f'priscilla{num}@gmail.com', password='*' * (num+1))
         for num in range(5)]
    groups: List[Group] = \
        [Group(name=f'grp{num}', description=f'grp{num}',
               created_by=users[num], users={users[num]})
         for num in range(3)]

    for user in users:
        auth_service._add_user(user)
    auth_service.commit()
    for group in groups:
        group_service._add_group(group)
    group_service.commit()

    # when first user is part of others groups
    group_service.add_user_to_group(
        user=users[2], guest=users[0], group=groups[2])
    group_service.commit()

    # then the expected groups are retrieved from get_user_groups
    user_groups: List[Group] = group_service.get_groups(user=users[0])
    assert user_groups == [groups[0], groups[2]]


def test_member_addition_by_a_member_is_accepted(auth_and_group_service):
    auth_service, group_service = auth_and_group_service
    # given some groups and users
    users: List[User] = \
        [User(email=f'priscilla{num}@gmail.com', password='*' * (num+1))
         for num in range(3)]
    group: Group = Group(name='grp', description='grp', created_by=users[0],
                         users=set(users[:2]))

    for user in users:
        auth_service._add_user(user)
    auth_service.commit()
    group_service._add_group(group)
    group_service.commit()

    # when request to add a user to a group is made by a current user
    group_service.add_user_to_group(user=users[1], guest=users[2], group=group)
    group_service.commit()

    # then group users return the added user and the previous ones
    assert set(group.users) == set(users)


def test_private_groups_are_not_visible_to_others(auth_and_group_service):
    auth_service, group_service = auth_and_group_service
    # given an existing group, that is private
    user0: User = User(email='priscilla@gmail.com', password='*-_f-')
    user1: User = User(email='miaoum@gmail.com', password='*f-')
    group: Group = Group(
        name='grp', description='grp', created_by=user0, is_private=True,
        users={user0, user1})
    auth_service._add_user(user0)
    auth_service.commit()
    group_service._add_group(group)
    group_service.commit()

    # when no user requests the existing groups
    # then the private group is not visible
    groups_visible_to_all = group_service.get_groups()
    assert not groups_visible_to_all
    assert group not in groups_visible_to_all

    # when another user requests the existing groups
    # then the private groupe is visible
    groups_visible_to_user1 = group_service.get_groups(user=user1)
    assert groups_visible_to_user1
    assert group in groups_visible_to_user1


def test_member_addition_by_non_member_is_refused(auth_and_group_service):
    auth_service, group_service = auth_and_group_service
    # given some groups and users
    users: List[User] = \
        [User(email=f'priscilla{num}@gmail.com', password='*' * (num+1))
         for num in range(4)]
    group: Group = Group(name='grp', description='grp', created_by=users[0],
                         users=set(users[:2]))

    for user in users:
        auth_service._add_user(user)
    auth_service.commit()
    group_service._add_group(group)
    group_service.commit()

    # when request to add a user to a group is made by a current user
    # then it is refused, and group users is not updated
    with pytest.raises(MembershipError):
        group_service.add_user_to_group(
            user=users[2], guest=users[3], group=group)
    assert users[2] not in group.users
    assert users[3] not in group.users
