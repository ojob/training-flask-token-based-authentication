import pytest

from jwt_authapp.domain.models import User
from jwt_authapp.domain.token import TokenType
from jwt_authapp.repository import NoResultFound
from jwt_authapp.services import auth


# ------------------------------------------------------------------- tests
def test_user_creation(auth_service):
    # given some user credentials
    user_data = dict(
        email='robert@mamadoo.se',
        password='ahah c\'est caché !!',
    )

    # when AuthService is requested to create a User instance
    user: User = auth_service.create_user(**user_data)

    # then this instance is created, with same email but hashed password
    assert user.email == user_data['email']
    assert user.password != user_data['password'], "password shall be hashed"


def test_user_access(auth_service):
    # given a User instance is created through the AuthService
    user_data = dict(
        email='robert@mamadoo.se',
        password='ahah c\'est caché !!',
    )
    initial_user: User = auth_service.create_user(**user_data)

    # when the AuthService is requested to return the created instance
    user: User = auth_service.get_user(email=user_data['email'])

    # then the expected User instance is returned
    assert user == initial_user


def test_user_rejection_of_wrong_request(auth_service):
    # given nothing

    # when the AuthService is requested for a user, but without criteria
    # then request is rejected
    with pytest.raises(TypeError):
        auth_service.get_user()


def test_auth_service_rollback_for_user_creation(auth_service):
    # given a User instance is created through the AuthService
    user_data = dict(
        email='robert@mamadoo.se',
        password='ahah c\'est caché !!',
    )
    user: User = auth_service.create_user(**user_data)

    # when the AuthService is requested to roll back
    auth_service.rollback()

    # then the user is not stored in the instance
    with pytest.raises(NoResultFound):
        auth_service.get_user(user_id=user.id)
    with pytest.raises(NoResultFound):
        auth_service.get_user(email=user_data['email'])


def test_user_login_success(auth_service):
    # given a user is registered
    user_data = dict(
        email='robert@mamadoo.se',
        password='ahah c\'est caché !!',
    )
    initial_user: User = auth_service.create_user(**user_data)

    # when user logs in using credentials to retrieve corresponding token
    token: TokenType = auth_service.log_user(**user_data)

    # then the initial user instance can be retrieved with the token
    user: User = auth_service._get_user_by_token(token)
    assert user == initial_user


def test_user_login_failure(auth_service):
    # given a user is registered
    user_data = dict(
        email='robert@mamadoo.se',
        password='ahah c\'est caché !!',
    )
    auth_service.create_user(**user_data)

    # when user logs in using other credentials
    # then the request fails with exception
    with pytest.raises(auth.UserLoginFailure):
        auth_service.log_user(email=user_data['email'], password='something')
    with pytest.raises(auth.UserLoginFailure):
        auth_service.log_user(
            email='some@mail.com', password=user_data['password'])


def test_user_logout(auth_service):
    # given a user is logged in
    user_data = dict(
        email='mamadou@robert.se',
        password='sekrreeeet',
    )
    user: User = auth_service.create_user(**user_data)
    token: TokenType = auth_service.get_token(user)

    # when user logs out with the retrieved token
    auth_service.blacklist(token)

    # then its token is blacklisted
    assert auth_service.is_blacklisted(token)
    with pytest.raises(auth.BlacklistedToken):
        auth_service.validate(token)

    # then trying to blacklist it again raise no failure (idempotency)
    auth_service.blacklist(token)
