import pytest

from jwt_authapp.domain import models


def test_user_creation():
    # given a user factory
    user_factory = models.UserFactory()

    # when the factory is used to create a new user
    user = user_factory.create(
        email='some@ware.com',
        password='my super #3)44'
    )

    # then instance is created, with hashed password
    assert user.email == 'some@ware.com'
    assert user.password != 'my super #3)44'
    assert user.registered_on
    assert not user.is_admin


def test_user_comparison_not_equal():
    # given two user instances with same email address
    user_1 = models.User(email='bob@there.net', password='1234')
    user_2 = models.User(email='bob@there.net', password='5243')
    not_a_user = {'id': user_1.id}

    # when there are compared to each other
    # then comparison is False, as only the id shall be used for comparison
    assert user_1 != user_2

    # when compared to something else
    # then comparison shall return False
    assert user_1 != not_a_user

    # when sorting is requested between two user instances
    # then it works by id
    assert (user_1 > user_2) == (user_1.id > user_2.id)

    # when sorting is requested between user and something else
    # then no sorting is performed
    with pytest.raises(TypeError):
        user_1 > 1343


def test_user_comparison_equal():
    # given a user
    user_1 = models.User('eby@eay.com', '0000')

    # when this user is replicated
    user_2 = models.User(
        id=user_1.id,
        email=user_1.email,
        password=user_1.password,
        registered_on=user_1.registered_on,
    )

    # then these User instances are compared equal
    assert user_1 == user_2
    assert hash(user_1) == hash(user_2)
