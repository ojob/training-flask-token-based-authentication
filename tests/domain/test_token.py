import jwt
import pytest

from jwt_authapp.domain import token as token_module


def test_user_token_management():
    # given some key and user id
    token_manager = token_module.TokenManager(secret_key='-- NOT SO SECRET --')

    # when a token is created by the token manager
    user_id = 234801
    token = token_manager.encode(user_id=user_id)

    # then token manager allows for decoding
    assert token_manager.decode(token)['sub'] == user_id


def test_user_token_expiration():
    # given the token manager is created with a negative validity duration
    token_manager = token_module.TokenManager(
        secret_key='-- NOT SO SECRET --', validity_duration=-5)

    # when the token manager is used to encode some user id
    token = token_manager.encode(124193)

    # then the token manager identifies that the token is expired on decoding
    with pytest.raises(token_module.TokenError):
        token_manager.decode(token)


def test_user_token_invalidity():
    # given some token manager
    token_manager = token_module.TokenManager(secret_key='-- NOT SO SECRET --')

    # when a invalid token is provided
    fake_token = 'whatever!'

    # then the token manager identifies that the token is invalid on decoding
    with pytest.raises(jwt.InvalidTokenError):
        token_manager.decode(fake_token)
