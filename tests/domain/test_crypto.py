from jwt_authapp.domain import crypto, models


def test_password_handling():
    # given a password
    pw_manager = crypto.FakePasswordManager()
    clear_password = 'some V3ry N1ce password'

    # when the hash is computed
    password_hash = pw_manager.hash(clear_password)

    # then the password check behaves as expected
    assert pw_manager.check(clear_password, password_hash)
    assert not pw_manager.check('some trial', password_hash)


def test_user_password_checking():
    # given some password
    user_factory = models.UserFactory()
    some_password = 'my super #3)44'

    # when user instance is created with this password
    user = user_factory.create(
        email='some@ware.com',
        password=some_password,
    )

    # then user factory provides a way to check against this password
    assert user_factory.check_user_password(user, some_password)
    assert not user_factory.check_user_password(user, '-docfd-civb-')
