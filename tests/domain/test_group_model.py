import pytest

from jwt_authapp.domain import models


def test_group_creation():
    # given a group factory and a user
    user_factory = models.UserFactory()
    user = user_factory.create(
        email='some@ware.com',
        password='my super #3)44'
    )
    group_factory = models.GroupFactory()
    group_data = dict(
        name='first group',
        description='Some first group !',
    )

    # when the factory is used to create a new group for this user
    group = group_factory.create(**group_data, created_by=user)

    # then group is created, with user attached as creator and sole user
    assert group is not None
    assert group.created_by is user
    assert group.users == {user}


@pytest.mark.parametrize(
    ('group1_data', 'group2_data', 'exp_comparison'),
    [
        (
            dict(id=1413, name='G1', created_on='1234'),
            dict(id=1413, name='G2', created_on='1234'),
            True,
        ),
        (
            dict(id=1413, name='G1', created_on='1234'),
            dict(id=2, name='G2', created_on='1234'),
            False,
        ),
        (
            dict(id=1413, name='G1', created_on='5131'),
            dict(id=1413, name='G2', created_on='1234'),
            False,
        ),
    ]
)
def test_equality_between_groups(group1_data, group2_data, exp_comparison):
    # given a user and two groups
    user_factory = models.UserFactory()
    user = user_factory.create(
        email='some@ware.com',
        password='my super #3)44'
    )
    group_factory = models.GroupFactory()
    group1: models.Group = group_factory.create(**group1_data, created_by=user)
    group2: models.Group = group_factory.create(**group2_data, created_by=user)

    # when they are compared
    # then result is equal iif they have same id and creation date
    assert (group1 == group2) == exp_comparison

    # when a group is compared to something else
    # then is is not equal
    assert not group1 == dict(id=group1.id)
