import pytest

from jwt_authapp import utils


@pytest.mark.parametrize(
    ('kwargs', 'expected'),
    [
        (dict(head='found', count=1, label='user', tail='in database'),
         'found 1 user in database'),
        (dict(count=3, label='foot', plural='feet'),
         '3 feet'),
        (dict(count=0, label='bob'),
         'no bob'),
    ],
)
def test_build_msg(kwargs, expected):
    assert utils.build_msg(**kwargs) == expected
