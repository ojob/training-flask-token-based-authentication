"""This module checks correct setup of authentication entry points."""

import pytest


# ------------------------------------------------------------------- tests
@pytest.mark.parametrize(
    ('entry_point', 'method'),
    [
        ('/users/', 'get'),
        ('/users/me', 'get'),
        ('/groups/', 'post'),
        ('/groups/mine', 'get'),
        # ('/groups/0/users', 'post'),  # TODO: how to test this?
        ('/auth/logout', 'post'),
    ],
)
def test_user_view_access_not_allowed_with_no_authorization_header(
        test_client, entry_point: str, method: str):
    # given a void case

    # when request for user page is sent but without Authorization header
    response = getattr(test_client, method)(entry_point)

    # then there is indication of failure
    assert response.status_code == 401  # Unauthorized
