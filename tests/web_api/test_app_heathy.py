def test_healthy_endpoint(test_client):
    # given nothing special

    # when requesting the app healthiness status
    response = test_client.get('/healthy')

    # then good health is returned
    assert response.status_code == 200
    assert response.json['message'] == 'up and running!'
