from helpers import register_user_and_return_token


def test_users_listing(test_client):
    # given a user is authenticated
    user_data = dict(
        email='amanda@some.where',
        password='do-cf-df-f',
    )
    token = register_user_and_return_token(test_client, user_data)

    # when users listing is requested
    response = test_client.get(
        '/users/',
        headers=dict(Authorization=f'Bearer {token}'),
    )

    # then created user is displayed
    assert response.status_code == 200
    assert response.json['message'] == "found 1 user"
    assert len(response.json['data']) == 1
    assert response.json['data'][0]['email'] == user_data['email']


def test_user_view_access(test_client):
    # given a user is registered
    user_data = dict(
        email='amanda@some.where',
        password='do-cf-df-f',
    )
    token = register_user_and_return_token(test_client, user_data)

    # when user status is requested with the token as authentication mean
    response = test_client.get(
        '/users/me',
        headers=dict(Authorization=f'Bearer {token}')
    )

    # then there is indication of success, with user details
    assert response.status_code == 200
    assert 'id' in response.json['data']
    assert response.json['data']['email'] == user_data['email']
    assert 'registered_on' in response.json['data']
    assert response.json['data']['is_admin'] in (True, False)
