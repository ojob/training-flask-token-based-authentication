import pytest

from jwt_authapp import create_app

from helpers import login, logout, register_user, register_user_and_return_token


# ------------------------------------------------------- specific fixtures
@pytest.fixture
def test_client_with_negative_token_validity():
    return create_app(token_validity=-1).test_client()


# ------------------------------------------------------------------- tests
def test_registration(test_client):
    # given valid user credentials
    user_data = dict(
        email='robert@wanadoo.fr',
        password='123456',
    )

    # when request for user registration is sent
    response = register_user(test_client, user_data)

    # then there is indication of success, with an authentication token
    assert response.status_code == 201
    assert response.json['message'] == "user created"
    assert 'token' in response.json['data']
    assert response.json['data']['token']


def test_registration_duplicate(test_client):
    # given valid user credentials, used once for registration
    user_data = dict(
        email='gérard@wanadoo.fr',
        password='123456',
    )
    register_user(test_client, user_data)

    # when request for registration is sent again
    response = register_user(test_client, user_data)

    # then there is indication of failure, with descriptive message
    assert response.status_code == 403, \
        "Duplicate registration shall be refused by 403 Forbidden"
    assert response.json['message'] == \
           "user already registered with this email"
    assert 'data' not in response.json


def test_login_by_credentials_success(test_client):
    # given a user is registered
    user_data = dict(
        email='bob@some.where',
        password='abcdABCD',
    )
    register_user(test_client, user_data)

    # when same credentials are used for logging in
    response = login(test_client, user_data)

    # then there is indication of success, with authentication token
    assert response.status_code == 200, \
        "user shall be able to log in with correct credentials"
    assert response.json['message'] == "successfully logged in"
    assert 'token' in response.json['data']
    assert response.json['data']['token']


def test_status_by_valid_token(test_client):
    # given a user is registered
    user_data = dict(
        email='amanda@some.where',
        password='do-cf-df-f',
    )
    token = register_user_and_return_token(test_client, user_data)

    # when user status is requested with the token as authentication mean
    response = test_client.get(
        '/auth/status',
        headers=dict(Authorization=f'Bearer {token}')
    )

    # then there is indication of success, with 'sub' entry (the token payload)
    assert response.status_code == 200
    assert 'sub' in response.json['data']


@pytest.mark.parametrize(
    "auth_fmt",
    [
        'Bearer:{}',
        'Bearer: {}',
        '{}',
        'bearer{}',
    ]
)
def test_status_with_invalid_authorization_format(test_client, auth_fmt):
    # given a user is registered
    user_data = dict(
        email='michaela@another.loc',
        password='----',
    )
    token = register_user_and_return_token(test_client, user_data)

    # when user status is requested with wrong Authorization header format
    response = test_client.get(
        '/auth/status',
        headers=dict(Authorization=auth_fmt.format(token))
    )

    # then there is indication of failure, with descriptive message
    assert response.status_code == 400  # Bad Request
    assert response.json['message'] == \
        "Authorization header format error; shall be like 'Bearer <token>'"


def test_status_without_authorization_content(test_client):
    # given a user is registered
    user_data = dict(
        email='michaela-2@another.loc',
        password='-__-',
    )
    register_user(test_client, user_data)

    # when user status is requested with no token in Authorization header
    response = test_client.get(
        '/auth/status',
        headers=dict(Authorization='Bearer ')
    )

    # then there is indication of failure, with descriptive message
    assert response.status_code == 400  # Bad Request
    assert response.json['message'] == \
        "Authorization header contains no token; " \
        "shall be like 'Bearer <token>'"


def test_status_without_authorization_header(test_client):
    # given a user is registered
    user_data = dict(
        email='angela@another.loc',
        password='o_0',
    )
    register_user(test_client, user_data)

    # when user status is requested with no Authorization header
    response = test_client.get(
        '/auth/status',
        headers=dict()  # no Authorization entry
    )

    # then there is indication of failure, with descriptive message
    assert response.status_code == 401  # Unauthorized
    assert response.json['message'] == \
        "Authorization header required for this resource but not present; " \
        "please add it with the token, like 'Bearer <token>'"


def test_status_by_wrong_token(test_client):
    # given a user is registered
    user_data = dict(
        email='jonas@some.where',
        password='doc=-\tidofid["',
    )
    token = register_user_and_return_token(test_client, user_data)
    invalid_token = ' ' + token

    # when user status is requested with invalid token as authentication mean
    response = test_client.get(
        '/auth/status',
        headers=dict(Authorization=f'Bearer {invalid_token}')
    )

    # then there is indication of failure, with descriptive message
    assert response.status_code == 401
    assert response.json['message'] == \
        'Token is invalid and cannot be used; please log in'
    assert 'data' not in response.json


def test_login_by_credentials_failure_with_wrong_password(test_client):
    # given a user is registered
    user_data = dict(
        email='bob@some.where',
        password='abcdABCD',
    )
    register_user(test_client, user_data)
    unknown_user_data = dict(
        email=user_data['email'],
        password="some wrong password",
    )

    # when user log in is requested with same email but with wrong password
    response = login(test_client, unknown_user_data)

    # then there is indication of failure, with elusive message
    assert response.status_code == 401
    assert response.json['message'] == "unknown email or wrong password"
    assert 'data' not in response.json


def test_logout_within_token_validity(test_client):
    # given a user is registered
    user_data = dict(
        email='bob-bob-bob@some.where',
        password='__///WWWW',
    )
    token = register_user_and_return_token(test_client, user_data)

    # when user logout is requested with token as authentication mean
    response = test_client.post(
        '/auth/logout',
        headers=dict(Authorization=f'Bearer {token}')
    )
    # then there is indication of success
    assert response.status_code == 200

    # when user logout is requested again with same token
    response = test_client.get(
        '/auth/status',
        headers=dict(Authorization=f'Bearer {token}')
    )
    # then there is indication of failure
    assert response.status_code == 401  # Unauthorized
    assert response.json['message'].startswith('Token is blacklisted')


def test_logout_out_of_token_validity(
        test_client_with_negative_token_validity,
):
    # given user is registered
    user_data = dict(
        email='mélanie@ici.ailleurs',
        password='---****',
    )
    token = register_user_and_return_token(
        test_client_with_negative_token_validity, user_data)

    # when user logout is requested with expired token
    # (no need to wait, as token validity is negative with current test client)
    response = logout(test_client_with_negative_token_validity, token)

    # then there is indication of failure
    assert response.status_code == 401  # Unauthorized
    assert response.json['message'] == \
        'Token is expired and cannot be used anymore; please log in'


def test_logout_with_blacklisted_token(test_client):
    # given user is registered and logged out
    user_data = dict(
        email='Jessica@ici.ailleurs',
        password='---****222',
    )
    token = register_user_and_return_token(test_client, user_data)
    logout(test_client, token)

    # when user logout is requested again with same token
    response = logout(test_client, token)

    # then there is indication of failure
    assert response.status_code == 401  # Unauthorized
    assert response.json['message'] == \
        'Token is blacklisted and cannot be used anymore; please log in'
