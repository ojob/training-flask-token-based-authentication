import json
from typing import Tuple

from flask import Response

from jwt_authapp.domain.token import TokenType


def register_user(test_client, user_data: dict) -> Response:
    return test_client.post(
        '/auth/register',
        data=json.dumps(user_data),
        content_type='application/json',
    )


def register_user_and_return_payload(
        test_client,
        user_data: dict
) -> Tuple[int, str]:
    response = register_user(test_client, user_data)
    return response.json['data']['id'], response.json['data']['token']


def register_user_and_return_token(test_client, user_data: dict) -> str:
    payload = register_user_and_return_payload(test_client, user_data)
    return payload[1]


def register_user_and_return_id(test_client, user_data: dict) -> int:
    payload = register_user_and_return_payload(test_client, user_data)
    return payload[0]


def login(test_client, user_data: dict) -> Response:
    return test_client.post(
        '/auth/login',
        data=json.dumps(user_data),
        content_type='application/json',
    )


def logout(test_client, token: TokenType) -> Response:
    return test_client.post(
        '/auth/logout',
        headers=dict(Authorization=f'Bearer {token}')
    )


def create_group(test_client, group_data: dict, token: TokenType) -> Response:
    return test_client.post(
        '/groups/',
        data=json.dumps(group_data),
        content_type='application/json',
        headers=dict(Authorization=f'Bearer {token}')
    )


def create_group_and_return_id(
        test_client,
        group_data: dict,
        token: TokenType,
) -> int:
    response: Response = create_group(test_client, group_data, token)
    return response.json['data']['id']


def add_user_to_group(
        test_client,
        group_id: int,
        user_id: int,
        token: TokenType,
) -> Response:
    return test_client.post(
        f'/groups/{group_id}/users',
        json=dict(user_id=user_id),
        headers=dict(Authorization=f'Bearer {token}')
    )
