from helpers import add_user_to_group, \
    create_group, create_group_and_return_id, \
    register_user_and_return_id, \
    register_user_and_return_token, register_user_and_return_payload


# ------------------------------------------------------------------- tests
def test_no_groups_are_displayed_initially(test_client):
    # given an instance with no groups

    # when list of groups is requested
    response = test_client.get('/groups/')

    # then no group is returned
    assert response.status_code == 200
    assert response.json['message'] == "no group found"
    assert response.json['data'] == []


def test_non_existing_group_display(test_client):
    # given an instance with no group and some unknown group id
    unknown_group_id = 120197911312

    # when group details are requested
    response = test_client.get(f'/groups/{unknown_group_id}')

    # then failure is returned
    assert response.status_code == 404
    assert response.json['message'] == \
        f"no group found with id={unknown_group_id}"

    # given there is an authenticated user
    user_data = dict(email='bob@bab.bib', password='1234')
    token = register_user_and_return_token(test_client, user_data=user_data)

    # when users for a non-existing group are requested
    response = test_client.get(
        f'groups/{unknown_group_id}/users',
        headers=dict(Authorization=f'Bearer {token}'),
    )

    # then failure is returned
    assert response.status_code == 404
    assert response.json['message'] == \
        f"no group found with id={unknown_group_id}"


def test_group_creation_and_retrieval(test_client):
    # given some group data and an existing user
    user_data = dict(email='bob@bab.bib', password='1234')
    token = register_user_and_return_token(test_client, user_data=user_data)
    group_data = dict(
        name='first group',
        description='A first group for this instance',
    )

    # when the group data is used to request creation of a new Group instance
    response = create_group(test_client, group_data, token)

    # then there is indication of success, with id of the newly created group
    assert response.status_code == 201  # Created
    assert response.json['message'] == "group created"
    assert 'data' in response.json
    assert 'id' in response.json['data']

    # ... and the group is listed in the groups, with expected details
    response = test_client.get('/groups/')
    assert response.status_code == 200
    assert response.json['message'] == '1 group found'
    assert isinstance(response.json['data'], list)
    assert response.json['data'][0]['name'] == 'first group'
    assert response.json['data'][0]['description'] == group_data['description']
    assert 'users' not in response.json['data'][0]


def test_group_details_display(test_client):
    # given a group exists
    user_data = dict(email='bob@bab.bib', password='1234')
    token = register_user_and_return_token(test_client, user_data=user_data)
    group_data = dict(
        name='first group',
        description='A first group for this instance',
    )
    group_id: int = create_group_and_return_id(test_client, group_data, token)

    # when details about its users is requested
    response = test_client.get(f'/groups/{group_id}')

    # the users are listed
    assert response.status_code == 200
    assert isinstance(response.json['data'], dict)
    assert 'created_on' in response.json['data']
    assert response.json['data']['created_by']['email'] == user_data['email']
    assert 'password' not in response.json['data']['created_by']
    assert 'users' not in response.json['data']


def test_group_users_addition_and_display(test_client):
    # given a group...
    user1_data = dict(email='bob@bab.bib', password='1234')
    user1_id, token1 = register_user_and_return_payload(
        test_client, user_data=user1_data)
    group_id: int = create_group_and_return_id(
        test_client,
        group_data=dict(name="group 1", description="First Group"),
        token=token1,
    )
    # ... with a second user as a member
    user2_data = dict(email='camille@nt.net', password='4123')
    user2_id = register_user_and_return_id(test_client, user2_data)
    test_client.post(
        f'/groups/{group_id}/users',
        json=dict(user_id=user2_id),
        headers=dict(Authorization=f'Bearer {token1}'),
    )

    # when users list is requested from the group
    response = test_client.get(
        f'/groups/{group_id}/users',
        headers=dict(Authorization=f'Bearer {token1}'),
    )

    # then all users are listed
    assert response.status_code == 200
    assert response.json['message'] == "found 2 users"
    assert len(response.json['data']) == 2
    assert set(user['id'] for user in response.json['data']) == \
        {user1_id, user2_id}


def test_addition_of_non_existing_user_to_group(test_client):
    # given an existing group
    user_data = dict(email='habib@bab.bib', password='1234')
    token = register_user_and_return_token(test_client, user_data=user_data)
    group_data = dict(
        name='first group',
        description='A first group for this instance',
    )
    group_id = create_group_and_return_id(test_client, group_data, token)

    # when trying to add a non-existing user to the group
    unknown_user_id = 120197911312
    response = add_user_to_group(test_client, group_id, unknown_user_id, token)

    # then there is indication of error, identifying the non-existing user
    assert response.status_code == 404
    assert response.json['message'] == \
        f"no user found with id={unknown_user_id}"


def test_display_of_my_groups(test_client):
    # given several users and groups exist
    user1_data = dict(email='habib@bab.bib', password='1234')
    user2_data = dict(email='khouldod@btun.is', password='4321')
    token1 = register_user_and_return_token(test_client, user_data=user1_data)
    user2_id, token2 = register_user_and_return_payload(test_client, user2_data)
    group1_data = dict(
        name='first group',
        description='A first group for this instance',
    )
    group2_data = dict(
        name='second group',
        description='Another group for this instance',
    )
    group1_id = create_group_and_return_id(test_client, group1_data, token1)
    create_group_and_return_id(test_client, group2_data, token2)

    # when a user is associated to a group they did not create
    response = add_user_to_group(test_client, group1_id, user2_id, token1)

    # then there is indication of success
    assert response.status_code == 200
    assert response.json['message'] == \
        f"user {user2_data['email']} associated to group {group1_data['name']}"

    # then a request of user's groups shows it as well
    response = test_client.get(
        '/groups/mine',
        headers=dict(Authorization=f'Bearer {token2}'),
    )
    assert response.status_code == 200
    assert response.json['message'] == 'user is part of 2 groups'
    group_names = set(entry['name'] for entry in response.json['data'])
    exp_names = {group1_data['name'], group2_data['name']}
    assert group_names == exp_names
