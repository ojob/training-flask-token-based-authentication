import pytest

from jwt_authapp import create_app


@pytest.fixture
def dev_app():
    app = create_app()
    app.config.from_object('jwt_authapp.config.DevelopmentConfig')
    return app


@pytest.fixture
def test_app():
    app = create_app()
    app.config.from_object('jwt_authapp.config.TestingConfig')
    return app


@pytest.fixture
def prod_app():
    app = create_app()
    app.config.from_object('jwt_authapp.config.ProductionConfig')
    return app


def test_app_is_development(dev_app):
    assert dev_app.config['DEBUG'] is True
    assert dev_app.config['SQLALCHEMY_DATABASE_URI'] == \
        'postgresql://postgres:@localhost/flask_jwt_auth'


def test_app_is_testing(test_app):
    assert test_app.config['DEBUG'] is True
    assert test_app.config['SQLALCHEMY_DATABASE_URI'] == \
        'postgresql://postgres:@localhost/flask_jwt_auth_test'


def test_app_is_production(prod_app):
    assert prod_app.config['DEBUG'] is False
