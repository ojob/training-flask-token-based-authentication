import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import clear_mappers, sessionmaker


from jwt_authapp import create_app
from jwt_authapp.repository.pupy import DictRepository
from jwt_authapp.repository.sql import SQLRepository
from jwt_authapp.repository.orm import metadata, start_mappers
from jwt_authapp.services import auth


@pytest.fixture
def in_memory_db():
    engine = create_engine('sqlite:///:memory:')
    metadata.create_all(engine)
    return engine


@pytest.fixture
def session(in_memory_db):
    start_mappers()
    yield sessionmaker(bind=in_memory_db)()
    clear_mappers()


@pytest.fixture
def repo(session):
    repo = SQLRepository(session=session)
    return repo


@pytest.fixture
def test_client(session):
    repo = SQLRepository(session=session)
    return create_app(repo=repo).test_client()


@pytest.fixture
def auth_service():
    repo = DictRepository()
    return auth.AuthService(repo)
