# Flask JWT Auth

This project is my implementation (with deep refactoring) of the
[RealPython Flask JWT Authentication tutorial][1], providing following
functionalities:

- user registration, with generation of a JSON Web Token;
- access to pages when a valid token is provided;
- ability to log out, by blacklisting the token.

[1]: https://realpython.com/token-based-authentication-with-flask/


## Preparing the field

1. clone the repo to a cosy place:

    ```sh
    $ cd some/nice/place
    $ git clone git@framagit.org:ojob/training-flask-token-based-authentication.git
    $ cd training-flask-token-based-authentication  
    ```

There is currently no real configuration setup; this will come in a future
update.


## Installing the application

2. create a new Python virtual environment, and activate it (the prompt is
  updated):

    ```sh
    $ python3 -m venv .env --prompt jwt-authapp
    $ source .env/bin/activate
    (jwt-authapp) $
    ```
  
3. update `pip` and install the Python package:

    ```sh
    (jwt-authapp) $ pip install --upgrade pip
    (jwt-authapp) $ pip install -e .
    ```
   
Notes:

- the dependencies are identified in `setup.py` file, and are installed 
  together with the package:

    - `flask` for the HTTP requests and responses handling
    - `bcrypt` for the passwords hashing
    - `pyjwt` for the JSON Web Token handling, i.e. encoding (including 
      signature) and decoding
    - `sqlalchemy` for data persistence management.  

- You really want to fire up a virtual environment, to avoid getting of
  dependencies that may break your system's Python installation.
- The versions of dependencies used by the developper are gathered in 
  `requirements-freeze.txt` file; in case of installation or testing failure,
  you may compare this file to the output of `pip freeze` command. 


## Testing the application

Install the test dependencies, and launch the test suite:

```sh
(jwt-authapp) $ pip install -r dev-requirements.txt
(jwt-authapp) $ pytest
```

where expected result is obviously to see following line at the end:

```sh
========= 25 passed, xx warnings in 18.41s =======
```


## Running the server

You may run the server as follows, if you want to play with it:

```sh
(jwt-authapp) $ python -m jwt_authapp
```

The server will run at http://127.0.0.1:5004/; the address and port will
become tunable in a future update.